# owncloud-animafac-theme

Custom ownCloud theme for [drive.animafac.net](https://drive.animafac.net/)

## Install

This theme need to be installed in the `themes/` folder of your ownCloud instance, then enabled in the ownCloud admin.

## Grunt tasks

[Grunt](https://gruntjs.com/) can be used to run some automated tasks defined in `Gruntfile.js`.

Your first need to install JavaScript dependencies with [Yarn](https://yarnpkg.com/):

```bash
yarn install
composer install
```

### Lint

You can check that the JavaScript, JSON, PHP and CSS files are formatted correctly:

```bash
grunt lint
```

## CI

[Gitlab CI](https://docs.gitlab.com/ee/ci/) is used to run some lint tasks automatically after each commit.
